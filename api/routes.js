module.exports = function(app) {
  const trainsController = require('./controllers/trainsController');
  const stationsController = require('./controllers/stationsController');

  app.get('/trains/:stationId', trainsController.getByStation)
  app.get('/autocomplete/:search', stationsController.autocomplete)
  app.get('/stations', stationsController.getAll)
}

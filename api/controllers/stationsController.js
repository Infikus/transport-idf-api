'use strict';

const mysql = require('mysql');
const { mysql: mysqlConfig } = require('../constants')

const connection = mysql.createConnection(mysqlConfig)

exports.autocomplete = (req, res) => {
  console.log(req.params);
  const search = normalize_string(req.params.search);

  const sql = 'SELECT * FROM station WHERE name_normalized LIKE ?';

  const query = connection.query(sql, ['%'+search+'%'], (err, results, fields) => {
    if (err) {
      res.status(500);
      res.send(err);
      return ;
    }

    res.send(results);
  });
console.log(query.sql);

}

function normalize_string(str) {
  const accents =
    "ÀÁÂÃÄÅĄàáâãäåąßÒÓÔÕÕÖØÓòóôõöøóÈÉÊËĘèéêëęðÇĆçćÐÌÍÎÏìíîïÙÚÛÜùúûüÑŃñńŠŚšśŸÿýŽŻŹžżź";
  const accentsOut =
    "AAAAAAAaaaaaaaBOOOOOOOOoooooooEEEEEeeeeeeCCccDIIIIiiiiUUUUuuuuNNnnSSssYyyZZZzzz";
  return str
    .split("")
    .map((letter, index) => {
      const accentIndex = accents.indexOf(letter);
      return accentIndex !== -1 ? accentsOut[accentIndex] : letter;
    })
    .join("").toUpperCase();
}


exports.getAll = (req, res) => {
  const sql = 'SELECT * FROM station';
  const query = connection.query(sql, (err, results, fields) => {
    if (err) {
      res.status(500);
      res.send(err);
      return ;
    }

    res.send(results);
  })
}

'use strict';

const request = require('request');
const mysql = require('mysql');
const { mysql: mysqlConfig, TRANSILIEN_API, TRANSILIEN_USER, TRANSILIEN_PASSWORD } = require('../constants')
const { parseString } = require('xml2js');

const connection = mysql.createConnection(mysqlConfig)

exports.getByStation = (req, res) => {
  const stationId = req.params.stationId;

  fetchNextTrains(stationId, (stations) => {
    console.log('success fetching ' + stationId);
    res.send(stations);
  }, (statusCode, error) => {
    console.log('error ' + statusCode);
    res.status(statusCode);
    res.send(error);
  });
}


function fetchNextTrains(stationId, onSuccess, onError) {
  request.get(`${TRANSILIEN_API}/gare/${stationId}/depart/`, {
    'auth': {
      'user': TRANSILIEN_USER,
      'password': TRANSILIEN_PASSWORD,
    }
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      parseString(body, (parseErr, passages) => {
        if (parseErr) return onError(500, parseErr);
        const stations = formatTransilienApiOutput(passages);

        findStationNames(stations, onSuccess, onError);
      })
    } else {
      return onError(response ? response.statusCode : 400, error);
    }
  })
}

function findStationNames(stations, onSuccess, onError) {
  // Query all stations
  const sql = 'SELECT * FROM station';

  // Cached uic names
  const uicResolved = [];

  connection.query(sql, (err, results, fields) => {
    if (err) return onError(500, err);
    const stationsWithName = stations.map(station => {
      const uic = station.term;
      let termName;

      if (uicResolved[uic]) termName = uicResolved[uic]
      else {
        const term = results.find(e => e.uic == uic);
        if (!term) termName = "Station non définie";
        else termName = term.name;
      }

      return {
        ...station,
        termName,
      }
    })
    onSuccess(stationsWithName);
  });

}

// Format the output of parseString to a beautiful json object
function formatTransilienApiOutput(passages) {
  if (!passages.passages.train) return [];
  return passages.passages.train.map(train => ({
    num: train.num[0],
    miss: train.miss[0],
    term: train.term[0],
    date: train.date[0]._,
    dateMode: train.date[0].$.mode
  }))
}


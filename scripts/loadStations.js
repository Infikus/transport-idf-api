const fs = require('fs');
const mysql = require('mysql');
const { mysql: mysqlConfig } = require('../api/constants');

console.log('*** START ***');

const connection = mysql.createConnection(mysqlConfig)

connection.connect();

const sql = 'INSERT INTO station (uic, name, name_normalized) VALUES ?';
var values = [];

const content = fs.readFileSync('gares.json');
const jsonContent = JSON.parse(content);

const array = jsonContent.map(obj => ({
  uic: parseInt(obj.fields.uic),
  name: obj.fields.intitule_gare
}));

const uicUniq = [];
const arrayUniq = array.sort((a, b) => a.uic < b.uic ? -1 : 1)
  .filter(a => {
    if (uicUniq.indexOf(a.uic) >= 0) return false;
    uicUniq.push(a.uic);
    return true;
  });

arrayUniq.forEach(obj => {
  values.push([
    obj.uic,
    obj.name,
    normalize_string(obj.name),
  ]);
})


connection.query(sql, [values], function(err) {
  if (err) throw err;
  connection.end();
})

function normalize_string(str) {
  const accents =
    "ÀÁÂÃÄÅĄàáâãäåąßÒÓÔÕÕÖØÓòóôõöøóÈÉÊËĘèéêëęðÇĆçćÐÌÍÎÏìíîïÙÚÛÜùúûüÑŃñńŠŚšśŸÿýŽŻŹžżź";
  const accentsOut =
    "AAAAAAAaaaaaaaBOOOOOOOOoooooooEEEEEeeeeeeCCccDIIIIiiiiUUUUuuuuNNnnSSssYyyZZZzzz";
  return str
    .split("")
    .map((letter, index) => {
      const accentIndex = accents.indexOf(letter);
      return accentIndex !== -1 ? accentsOut[accentIndex] : letter;
    })
    .join("").toUpperCase();
}


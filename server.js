const express = require('express');
const app = express();
const port = process.env.PORT || 3000;


require('./api/routes')(app);
app.listen(port);

console.log('Transport-temps-reel API started on ' + port);

